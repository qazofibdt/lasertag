#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdint.h>

void configureSystem(void);
// Shutdown all modules immediately
void shutdownSystem(void);
void delay(uint32_t d);
void delayTiny(uint32_t d);

#endif  // SYSTEM_H
